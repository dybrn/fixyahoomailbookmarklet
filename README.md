# README #

This is a bookmarklet I use to get rid of the annoying notification on yahoo mail about needing to pay to use the non-basic mail.

### What is this repository for? ###

* A place to store the bookmarklet and provide it to others that may use it.
* Version 1.0

### How do I get set up? ###

* Open the index.html page
* Follow the instructionw written on the index.html page.
* When yahoo mail randomly displays a modal dialog telling you to pay or get crippled, click the bookmarklet and profit.

### Contribution guidelines ###

* Don't break anything
* Only contribute things that make this better

### Who do I talk to? ###

* Repo owner or admin
